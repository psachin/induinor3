/*
  Remote control lamps with PIR & HC-SR04+Servo.
*/
#include <Servo.h>
Servo servo;

const int pin_servo = 10;
const int servo_idle_pos = 111;  // (magic number) This is where servo will finally listen on.

const int pin_lamp_1 = 11;
const int pin_lamp_2 = 12;
const int pin_turn_off_signal = 13;

const int pin_tsop = 15;      /* pin A1 */
const int pin_pir = 2;        /* pin A2 */
const int pingPin = 16;       // A2 - Connected to both Trig & ECHO Pins of the Sensor

int pir_low_counter = 0;
int hcsr04_low_counter = 0;

const int lamp_turn_off_delay = 60;  // X seconds delay before turning off the lamp(s)
const int hcsr04_turn_off_delay = 5; // Re-check count before turning off the lamp(s)

const int hcsr04_distance_cm = 120;   // Distance range (in CentiMeters)

bool lamp_1_state = false;
bool lamp_2_state = false;
bool pir_state = false;
bool warned = false;
bool lamp_turned_off = false;

long duration, inches, cm;

void setup() {
    // initialize
    servo.attach(pin_servo);

    /* servo angle checks */
    servo_check();

    /* find a position to start monitoring */
    look_around();

    pinMode(pin_lamp_1, OUTPUT);
    pinMode(pin_lamp_2, OUTPUT);
    pinMode(pin_turn_off_signal, OUTPUT);
    pinMode(pin_pir, INPUT);
    pinMode(pin_tsop, INPUT);
    Serial.begin(9600);
}

// the loop routine runs over and over again forever:
void loop() {
    int remote_val = remote();

    cm = ping_in_cm();
    Serial.print(cm);
    Serial.print(" cm\n");

    pir_state = digitalRead(pin_pir);
    Serial.print("PIR state: ");
    Serial.print(pir_state);
    Serial.print("\n");

    Serial.print("PIR low counter: "); /* Duration when the PIR is in low state */
    Serial.print(pir_low_counter);
    Serial.print("\n");
    Serial.print("HCSR04 low counter: "); /* HCSR04 low state count. i.e distance > hcsr04_distance_cm */
    Serial.print(hcsr04_low_counter);
    Serial.print("\n");

    if(remote_val > 0 && pir_state == HIGH) {
        Serial.println(remote_val);
        Serial.println(pir_state);

        if(remote_val == 128) {
            lamp_1_state = !lamp_1_state;
            Serial.print("Lamp 1 state: ");
            Serial.print(lamp_1_state);
            Serial.print("\n");
            digitalWrite(pin_lamp_1, lamp_1_state);
        }

        if(remote_val == 129) {
            lamp_2_state = !lamp_2_state;
            Serial.print("Lamp 2 state: ");
            Serial.print(lamp_2_state);
            Serial.print("\n");
            digitalWrite(pin_lamp_2, lamp_2_state);
        }

        if(remote_val == 151) {
            look_around();
        }
    }
    else if(pir_state == LOW && cm > hcsr04_distance_cm &&(lamp_1_state == 1 || lamp_2_state == 1)) {
        pir_low_counter++;	/* debounce */
        hcsr04_low_counter++;	/* debounce */

        /* Signal for 9 seconds before turning off the lamps */
        /* Move!! A chance to avoid turning off the lamps */
        if (pir_low_counter >= (lamp_turn_off_delay - 10) && warned == false) {
            for (int i = 0; i < 9; i++) {
                digitalWrite(pin_turn_off_signal, HIGH);
                delay(700);
                digitalWrite(pin_turn_off_signal, LOW);
                delay(300);
            }
            warned = true;
        }

        if ((pir_low_counter + 9) >= lamp_turn_off_delay && hcsr04_low_counter >= hcsr04_turn_off_delay) {
            turn_off();
            lamp_turned_off = true;
            pir_low_counter = 0;
            hcsr04_low_counter = 0;

            /* We cannot reset lamp(s) state to 0 because we need to restore the
             * state as soon as the PIR & HCSR04 values are high*/
        }
    }
    else if (pir_state == HIGH || pir_low_counter != 0 || cm < hcsr04_distance_cm || hcsr04_low_counter != 0) {
        if (lamp_turned_off) {
            restore_lamp_state(lamp_1_state, lamp_2_state);
        }

        pir_low_counter = 0;
        hcsr04_low_counter = 0;
        warned = false;
    }
}

void servo_check() {
    int pos = 0;
    pos = servo.read();

    for (pos; pos <= 180; pos++) {
        servo.write(pos);
        delay(10);
    }

    for (pos = 180; pos > 1; pos--) {
        servo.write(pos);
        delay(10);
    }
}

void look_around() {
    // and find a new idle position

    int pos = 0;
    pos = servo.read();
    int new_idle_pos = 0;
    int attempt = 0;

    /* reset to the start position */
    for (pos; pos > 10; pos--) {
        servo.write(pos);
        delay(10);
    }


    while (new_idle_pos == 0 && attempt < 2) {

        for (pos = 10; pos <= 180; pos++) {
            servo.write(pos);

            cm = ping_in_cm();
            Serial.print("CM: ");
            Serial.print(cm);
            Serial.print("\n");
            Serial.print("POS: ");
            Serial.print(pos);
            Serial.print("\n");
            delay(30);

            if (cm < hcsr04_distance_cm && cm > 70 && pos > 10 && pos < 180) {
                new_idle_pos = pos;  // New idle position.
            }
        }
        attempt++;
    }

    if (new_idle_pos != 0 && new_idle_pos != servo_idle_pos) {
        servo.write(new_idle_pos);
        Serial.print("New idle pos: ");
        Serial.print(new_idle_pos);
        Serial.print("\n");
    } else {
        Serial.println("-- Servo idle pos set --");
        servo.write(servo_idle_pos);
    }
}

void turn_off() {
    digitalWrite(pin_lamp_1, LOW);
    digitalWrite(pin_lamp_2, LOW);
}

void restore_lamp_state(bool lamp_1_state, bool lamp_2_state) {
    digitalWrite(pin_lamp_1, lamp_1_state);
    digitalWrite(pin_lamp_2, lamp_2_state);
    lamp_turned_off = 0;
    Serial.println(" ---- Restore ----");
    look_around();
}

int remote() {
    int value = 0;
    int time = pulseIn(pin_tsop, LOW);
    if(time>2000) {
        for(int counter=0; counter < 12; counter++) {
            if(pulseIn(pin_tsop, LOW) > 1000) {
                value = value + (1 << counter);
            }
        }
    }
    delay(250);                // A remote press will normally generate 3
                               // signal. Delay mitigates the noise.
    return value;
}

long ping_in_cm() {

    pinMode(pingPin, OUTPUT);
    digitalWrite(pingPin, LOW);
    delayMicroseconds(2);
    digitalWrite(pingPin, HIGH); // Sending a High Pulse on the Trig Pin of the Sensor
    delayMicroseconds(5);
    digitalWrite(pingPin, LOW);
    pinMode(pingPin, INPUT);
    duration = pulseIn(pingPin, HIGH);
    return microsecondsToCentimeters(duration);
}

long microsecondsToInches(long microseconds) {
    // According to Parallax's datasheet for the PING))), there are 73.746
    // microseconds per inch (i.e. sound travels at 1130 feet per second). This
    // gives the distance travelled by the ping, outbound and return, so we
    // divide by 2 to get the distance of the obstacle. See:
    // http://www.parallax.com/dl/docs/prod/acc/28015-PING-v1.3.pdf
    return microseconds / 74 / 2;
}

long microsecondsToCentimeters(long microseconds) {
    // The speed of sound is 340 m/s or 29 microseconds per centimeter. The ping
    // travels out and back, so to find the distance of the object we take half
    // of the distance travelled.
    return microseconds / 29 / 2;
}
